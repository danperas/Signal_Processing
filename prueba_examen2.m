
n = 0:99;

a = 1;
b = [1 -2 1];



x1 = cos(pi*n/10);
y1 = filter(b,a,x1);
x2 = cos(pi*n/2);
y2 = filter(b,a,x2);
x3 = cos(9*pi*n/10);
y3 = filter(b,a,x3);

figure;
subplot(311); stem(n,y1);
subplot(312); stem(n,y2);
subplot(313); stem(n,y3);