function [x]=EX(Om1,Om2,L,N);

[P1,Q1]=rat(Om1/(2*pi))
[P2,Q2]=rat(Om2/(2*pi))
M12=lcm(Q1,Q2)
M=ceil(L/M12)*M12
Tc=1
n=1:1/Tc:L-1;
x=cos(Om1*n)+cos(Om2*n);
XF=fft(x,N);
wp=0:2*pi/N:2*pi*(1-1/N);
w=wp/Tc;
N2=round(N/2);
plot(w(1:N2),abs(XF(1:N2)))
XF=fft(x,M);
wp=0:2*pi/M:pi*(1-2/M);
w=wp/Tc;
M22=round(M/2);
hold on
stem(w(1:M22), abs(XF(1:N2)))
hold off

%%% L = 2*M+1;
%%% n = n1:n2;
%%% i = 1-n1;
%%% x(i-M:i+M) = blackman(L);
%%% XF = fft(x,N);
%%% plot(w,)