%%%%%%%%%%%%%%%%%%%%%%%%% DANIEL PÉREZ ASENSIO %%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%    PRACTICA 3    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%    Ejercicio 3     %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%    Anáisis espectral de secuencias discretas mediante la DFT   %%%%

clc; close all; clear all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%     3.1     Análisis de una secuencia sinusoidal    %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% 3.1.1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% L es la longitud de la señal x[n].
% M es la lontitud entre la sucesión de x[n] de xm[n].

L = 50;     % Equivalente a usar una ventana de longitud 50.
n = 0:L-1;
x = cos(0.45*pi*n);
f = 0.45/(2*pi);
N = 4000;
X = fft(x, N);  
w = 0:2*pi/N:2*pi-2*pi/N;   

figure();
plot(w, abs(X)); xlabel '\Omega (rad)'; ylabel '|X[\Omega]|'; grid on;
axis([0 max(w) 0 ceil(max(abs(X)))]);   % ceil redondea hacia arriba.

%%%%% 3.1.2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% M >= L  Omega = k*(2*pi)/N donde k = 0....N-1
% Omega = 0.45pi -> 45*2pi/200 = k*2pi/4000 ; k=900 -> 9*2pi/40; k=9 y M=40
% Para que M >= L ; 40*2 >= 50; M = 80 y k = 18;

hold on;
M = 80;
X = fft(x, M);
w = 0:2*pi/M:2*pi-2*pi/M;
stem(w, abs(X));

% k1 = 19-1 y k2 = 64-1 -> k1 = 18 y k2 = 63. (Contando en la gráfica.) 
% k1 coincide con el k calculado antes. k2 es el resultado de  

%%%%% 3.1.3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N = 5000;
X = fft(x, N);
w = 0:(2*pi/N):(2*pi*(1-1/N));
figure();
plot(w, abs(X)); xlabel '\Omega (rad)'; ylabel '|X[\Omega]|'; grid on;
axis([0 max(w) 0 ceil(max(abs(X)))]);
hold on;
M = 80;
X = fft(x, M);
w = 0:2*pi/M:2*pi-2*pi/M;
stem(w, abs(X));

% No coinciden los máximos de las señales exactamente. A simple vista sí,
% pero al realizar zoom no. Lo que hemos hecho ha sido aumentar el valor de
% N, es decir, el número de muestras. Al hacer esto, tenemos más resolución
% en la señal y el lóbulo principal es más estrecho. Los máximos no
% coinciden porque K no es un número natural para N=4096.

%%%%% 3.1.4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
M = 80;
N = 4000;
L = M;
n = 0:L-1;
x = cos(0.45*pi*n);
X = fft(x, N);
w = 0:(2*pi/N):(2*pi*(1-1/N));
figure();
plot(w, abs(X)); xlabel '\Omega (rad)'; ylabel '|X[\Omega]|'; grid on;
axis([0 max(w) 0 ceil(max(abs(X)))]);
hold on;
X = fft(x, M);
w = 0:2*pi/M:2*pi-2*pi/M;
stem(w, abs(X));

% Se observa que los máximos de las señales coinciden completamente. Sin
% embargo, todos los demás valores están a 0. También se observa que la
% anchura del lóbulo principal es menor, al haber aumentado la longitud de
% la ventana. Las muestras coinciden exactamente con los mínimos de la
% señal, es decir, son 0; salvo en los lóbulos principales, que tienen el
% valor máximo. Este efecto se produce gracias a que M = L, la longitud de
% la ventana es igual al período de la secuencia de x[n].


%%% EN RESUMEN: primero, calculamos cuál es el valor mínimo del período
%%% (Nmin) de la sucesión de x[n] para garantizar que los máximos de la TF 
%%% coinciden con las muestras de la DFT. Segundo, calculamos cuál es el
%%% valor mínimo del período (M) de la sucesión de x[n] para garantizar que
%%% los máximos de la TF coinciden con las muestras de la DFT y además que
%%% no se produzca submuestreo a fin de poder recuperar la señal
%%% inicial. Tercero, cambiamos el valor de N a 4096 muestras en vez de
%%% 4000, que era el valor inicial. Esto nos produce una TF que no coincide
%%% con los máximos de la DFT. Esto es debido a que K no es un número
%%% natural (o entero positivo) para N=4096. Cuarto, volvemos a cambiar el
%%% valor de N a su estado inicial (N=4000). En este caso, igualamos el
%%% valor de L = M, de tal forma que la longitud de la ventana de x[n] sea
%%% idéntica a la longitud del período de la sucesión de x[n]. Observamos
%%% que obtenemos una exactitud total en los mínimos de la TF, donde la DFT
%%% es cero. En los máximos de la TF, la DFT es máxima.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%     3.2     Análisis de una suma de secuencias      %%%%%%%%%%%%%%%%
%%%%                        sinusoidales                             %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% 3.2.1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% x[n] = cos(0,4πn) + cos(2πn/3); L = 200; N = 4500;

L = 200;
n = 0:L-1;
x = cos(0.4*pi*n) + cos(2*pi*n/3);
N = 4500;

% Realizamos el cáculo para que M ≥ L:  
%
%   Ω = 2πk/N; 4*2π/15 = 2πk/4500; k = 1200; 1200 * 2π/4500; k = 4; M = 210; 
%
M = 210;
X = fft(x, N);
w = 0:(2*pi/N):(2*pi*(1-1/N));
figure();
plot(w, abs(X)); xlabel '\Omega (rad)'; ylabel '|X[\Omega]|'; grid on;
axis([0 max(w) 0 ceil(max(abs(X)))]); % ceil redondea hacia arriba.
hold on;
X = fft(x, M);
w = 0:(2*pi/M):(2*pi*(1-1/M));
stem(w, abs(X));

%%%%% 3.2.2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Para conseguir el mismo efecto que en el ejercicio 3.1.4 al cambiar L, lo
% que tenemos que hacer es igualar L = M, es decir, que el tamaño de la
% ventana de la suma de sinusoides coincida con el período de la secuencia
% de la suma de sinusoides. Por lo tanto: L = M; L = 210.

M = 210;
L = M;
n = 0:L-1;
x = cos(0.4*pi*n) + cos(2*pi*n/3);
N = 4500;
X = fft(x, N);
w = 0:(2*pi/N):(2*pi*(1-1/N));
figure();
plot(w, abs(X)); xlabel '\Omega (rad)'; ylabel '|X[\Omega]|'; grid on;
axis([0 max(w) 0 ceil(max(abs(X)))]); % ceil redondea hacia arriba.
hold on;
X = fft(x, M);
w = 0:(2*pi/M):(2*pi*(1-1/M));
stem(w, abs(X));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%    Ejercicio 4     %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%    Resolución y fugas espectrales   %%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%     4.1     Resolución espectral      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% 4.1.1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% x[n] = cos(0,45πn) + cos(0,55πn); L=10; N=1024;

% m.c.m.(45/100, 55/100) = 495 / 100 ?????

L = 10; N = 1024; n = 0:L-1;
x1 = cos(0.45*pi*n); X1 = fft(x1, N);
x2 = cos(0.55*pi*n); X2 = fft(x2, N);
x = x1 + x2;
w = 0:(2*pi/N):(2*pi*(1-1/N));
figure();
plot(w, abs(X1)); xlabel '\Omega (rad)'; ylabel '|X[\Omega]|'; grid on;
hold on;
plot(w, abs(X2));

% Dado que las señales se superponen, va a haber problemas a la hora de
% representar la suma de las dos señales. Se va a producir un solapamiento.
X = fft(x, N);
plot(w, abs(X));

% Observamos cómo la señal resultante de la suma produce únicamente un
% lóbulo principal, como si de una sola señal se tratase.

%%%%% 4.1.2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure();
N = 1024;w = 0:(2*pi/N):(2*pi*(1-1/N));X = fft(x, N); plot(w, abs(X));
xlabel '\Omega (rad)'; ylabel '|X[\Omega]|'; grid on;
figure();
N = 2048;w = 0:(2*pi/N):(2*pi*(1-1/N));X = fft(x, N);plot(w, abs(X));
xlabel '\Omega (rad)'; ylabel '|X[\Omega]|'; grid on;
figure();
N = 4096;w = 0:(2*pi/N):(2*pi*(1-1/N));X = fft(x, N);plot(w, abs(X));
xlabel '\Omega (rad)'; ylabel '|X[\Omega]|'; grid on;

% Por más muestras que utilizamos para representar la TF de suma de las
% señales, no obtenemos una mayor precisión a la hora de representar los
% dos lóbulos principales de las dos sinusoides. Aumentar las muestras en
% frecuencia no resulta una solución para nada.

%%%%% 4.1.3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

L = 15; N = 1024; n = 0:L-1;
x1 = cos(0.45*pi*n);
x2 = cos(0.55*pi*n);
x = x1 + x2;
w = 0:(2*pi/N):(2*pi*(1-1/N));
figure();
X = fft(x, N);
plot(w, abs(X)); xlabel '\Omega (rad)'; ylabel '|X[\Omega]|'; grid on;

L = 40; N = 1024; n = 0:L-1;
x1 = cos(0.45*pi*n);
x2 = cos(0.55*pi*n);
x = x1 + x2;
w = 0:(2*pi/N):(2*pi*(1-1/N));
figure();
X = fft(x, N);
plot(w, abs(X)); xlabel '\Omega (rad)'; ylabel '|X[\Omega]|'; grid on;

L = 70; N = 1024; n = 0:L-1;
x1 = cos(0.45*pi*n);
x2 = cos(0.55*pi*n);
x = x1 + x2;
w = 0:(2*pi/N):(2*pi*(1-1/N));
figure();
X = fft(x, N);
plot(w, abs(X)); xlabel '\Omega (rad)'; ylabel '|X[\Omega]|'; grid on;

L = 100; N = 1024; n = 0:L-1;
x1 = cos(0.45*pi*n);
x2 = cos(0.55*pi*n);
x = x1 + x2;
w = 0:(2*pi/N):(2*pi*(1-1/N));
figure();
X = fft(x, N);
plot(w, abs(X)); xlabel '\Omega (rad)'; ylabel '|X[\Omega]|'; grid on;


% En el valor de L=15 se puede observar que los lóbulos principales están
% totalmente separados. Hay que ver cómo hacer los cálculos teóricos.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%     4.2     Fugas Espectrales      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% 4.2.1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% x[n]=cos(0,3πn)+0,02cos(0,7πn);L=50;N=1024;
L=50;N=1024;
n=0:L-1;
x1=cos(0.3*pi*n);X1=fft(x1,N);
x2=0.02*cos(0.7*pi*n);X2=fft(x2,N);
x=x1+x2;X=fft(x,N);
w=0:(2*pi/N):(2*pi*(1-1/N));
figure();plot(w,abs(X1));xlabel '\Omega (rad)';ylabel '|X(\Omega)|';
grid on;hold on; plot(w,abs(X2));

% Es evidente que al observar la gráfica de las dos señales, una de ellas
% tiene una amplitud mucho menor que la otra, y que va a quedar enmascarada
% por la que tiene mayor amplitud, además de solaparse en los lóbulos
% secundarios.

figure();plot(w,abs(X));xlabel '\Omega (rad)';ylabel '|X(\Omega)|';
grid on;

%%%%% 4.2.2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

L=50;N=1024;
n=0:L-1;
x1=cos(0.3*pi*n);
x2=0.02*cos(0.7*pi*n);
x=x1+x2;X=fft(x,N);
w=0:(2*pi/N):(2*pi*(1-1/N));
figure();plot(w,abs(X));xlabel '\Omega (rad)';ylabel '|X(\Omega)|';
grid on;

L=50;N=2048;
n=0:L-1;
x1=cos(0.3*pi*n);
x2=0.02*cos(0.7*pi*n);
x=x1+x2;X=fft(x,N);
w=0:(2*pi/N):(2*pi*(1-1/N));
figure();plot(w,abs(X));xlabel '\Omega (rad)';ylabel '|X(\Omega)|';
grid on;

L=50;N=4096;
n=0:L-1;
x1=cos(0.3*pi*n);
x2=0.02*cos(0.7*pi*n);
x=x1+x2;X=fft(x,N);
w=0:(2*pi/N):(2*pi*(1-1/N));
figure();plot(w,abs(X));xlabel '\Omega (rad)';ylabel '|X(\Omega)|';
grid on;

% Al aumentar el número de muestras N para la representación de la TF de la
% señal, observamos que a mayor muestras no se produce ningún cambio ni
% mejora. No depende del número de muestras de representación. Va a
% depender del tamaño o longitud de la ventana y de su tipo.

%%%%% 4.2.3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

L=50;N=1024;
n=0:L-1;
x1=cos(0.3*pi*n);
x2=0.02*cos(0.7*pi*n);
x=x1+x2;X=fft(x,N);
w=0:(2*pi/N):(2*pi*(1-1/N));
figure();plot(w,abs(X));xlabel '\Omega (rad)';ylabel '|X(\Omega)|';
grid on;

L=80;N=1024;
n=0:L-1;
x1=cos(0.3*pi*n);
x2=0.02*cos(0.7*pi*n);
x=x1+x2;X=fft(x,N);
w=0:(2*pi/N):(2*pi*(1-1/N));
figure();plot(w,abs(X));xlabel '\Omega (rad)';ylabel '|X(\Omega)|';
grid on;

L=100;N=1024;
n=0:L-1;
x1=cos(0.3*pi*n);
x2=0.02*cos(0.7*pi*n);
x=x1+x2;X=fft(x,N);
w=0:(2*pi/N):(2*pi*(1-1/N));
figure();plot(w,abs(X));xlabel '\Omega (rad)';ylabel '|X(\Omega)|';
grid on;

% Se observa que al aumentar el tamaño de ventana, se hacen más evidentes
% los máximos de la señal que tiene menor amplitud, sin embargo, el
% solapamiento se sigue produciendo y por tanto, el problema de fugas no
% está resuelto.

%%%%% 4.2.4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

L=50;N=1024;
n=0:L-1;
M=(L-1)/2;
xn=cos(0.3*pi*n)+0.02*cos(0.7*pi*n);
X=fft(x,N);
w=0:(2*pi/N):(2*pi*(1-1/N));

%Ventana triangular
wt=[zeros(1,((length(n)-1)/2-M)) triang(L)'.*ones(1,L) zeros(1,((length(n)-1)/2-M))];
%Ventana Hamming
wh=[zeros(1,((length(n)-1)/2-M)) hamming(L)'.*ones(1,L) zeros(1,((length(n)-1)/2-M))];
%Ventana Blackman
wb=[zeros(1,((length(n)-1)/2-M)) blackman(L)'.*ones(1,L) zeros(1,((length(n)-1)/2-M))];

xn1=xn.*wt;
xn2=xn.*wh;
xn3=xn.*wb;

figure();plot(w,abs(X));xlabel '\Omega (rad)';ylabel '|X(\Omega)|';
title 'RECTANGULAR';grid on;

X=fft(xn1,N);
figure();plot(w,abs(X));xlabel '\Omega (rad)';ylabel '|X(\Omega)|';
title 'TRIANGULAR';grid on;

X=fft(xn2,N);
figure();plot(w,abs(X));xlabel '\Omega (rad)';ylabel '|X(\Omega)|';
title 'HAMMING';grid on;

X=fft(xn3,N);
figure();plot(w,abs(X));xlabel '\Omega (rad)';ylabel '|X(\Omega)|';
title 'BLACKMAN';grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%     4.3     Resolución y Fugas Espectrales      %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% 4.3.1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

L=10;N=1024;
n=0:L-1;
x1=cos(0.47*pi*n);X1=fft(x1,N);
x2=0.035*cos(0.53*pi*n);X2=fft(x2,N);
x=x1+x2;X=fft(x,N);
w=0:(2*pi/N):(2*pi*(1-1/N));
figure();plot(w,abs(X1));xlabel '\Omega (rad)';ylabel '|X(\Omega)|';
grid on;hold on;plot(w,abs(X2));



















